package ultraemojicombat;

import java.util.Random;

public class Luta {
    private lutador desafiado;
    private lutador desafiante;
    private int rouds;
    private boolean aprovada;
    public void marcarluta(lutador l1,lutador l2){
        if(l1.getCategoria()==l2.getCategoria()&&l1!=l2){
            this.aprovada=true;
            this.desafiado=l1;
            this.desafiante=l2;
        }
        else{
            this.aprovada=false;
            this.desafiado=null;
            this.desafiante=null;
        }
    }
    public  void lutar(){
        if(this.aprovada)
        {   System.out.println("=======================");
            System.out.println("#### desefiado ####");    
            this.desafiado.apresentar();
            System.out.println("### desafiante ####");
            this.desafiante.apresentar();
            Random aleatorio=new Random();
            int vencedor=aleatorio.nextInt(3);
                    switch(vencedor){
                        case 0:
                            System.out.println("empatou");
                            this.desafiado.empatarluta();
                            this.desafiante.empatarluta();
                            break;
                        case 1:
                            System.out.println("vencedor: "+this.desafiado.getNome());
                            this.desafiado.ganharluta();
                            this.desafiante.perderluta();
                            break;
                        case 2:
                            System.out.println("vencedor");
                            this.desafiado.perderluta();
                            this.desafiante.ganharluta();
                            break;
                    }
                    System.out.println("=============================");
        }
        else{
            System.out.println("luta nao pode acntecer:");
        }
    }

    

    public lutador getDesafiado() {
        return desafiado;
    }

    public void setDesafiado(lutador desafiado) {
        this.desafiado = desafiado;
    }

    public lutador getDesafinate() {
        return desafiante;
    }

    public void setDesafinate(lutador desafinate) {
        this.desafiante = desafinate;
    }

    public int getRouds() {
        return rouds;
    }

    public void setRouds(int rouds) {
        this.rouds = rouds;
    }

    public boolean getAprovada() {
        return aprovada;
    }

    public void setAprovada(boolean aprovada) {
        this.aprovada = aprovada;
    }
    
}
