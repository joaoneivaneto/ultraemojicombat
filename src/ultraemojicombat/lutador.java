
package ultraemojicombat;
public class lutador {
    private String nome;
    private String nacionalidade;
    private int idade;
    private float altura;
    private  float peso;
    private String categoria;
    private int vitorias;
    private int derrotas;
    private int empates;
    public void apresentar(){
        System.out.println("luador: "+this.getNome());
        System.out.println("origem: "+this.getNacionalidade());
        System.out.println(this.getIdade()+" anos");
        System.out.println(this.getAltura()+" M de altura");
        System.out.println("pesando: "+this.getPeso()+"kg");
        System.out.println("ganhou: "+this.getVitorias());
        System.out.println("perdeu: "+this.getDerrotas());
        System.out.println("empatou: "+this.getEmpates());
    
    }
    public void  status(){
        System.out.println(this.getNome());
        System.out.println("é um peso: "+this.getCategoria());
        System.out.println(this.getVitorias()+" vitorias");
        System.out.println(this.getDerrotas()+" derrotas");
        System.out.println(this.getEmpates()+ " empates");    
                
        
    }
    public void ganharluta(){
        this.setVitorias(this.getVitorias()+1);
    }
    public void perderluta(){
        this.setDerrotas(this.getDerrotas()+1);
    }
    public void empatarluta(){
        this.setEmpates(this.getDerrotas()+1);
    }

    public lutador(String nome, String nacionalidade, int idade, 
                    float altura, float peso,  
                    int vitorias, int derrotas, int empates) {
        this.nome = nome;
        this.nacionalidade = nacionalidade;
        this.idade = idade;
        this.altura = altura;
        this.setPeso(peso);
        this.categoria = categoria;
        this.vitorias = vitorias;
        this.derrotas = derrotas;
        this.empates = empates;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    @Override
    public String toString() {
        return "lutador{" + "nome=" + nome + ", nacionalidade=" + nacionalidade + ", idade=" + idade + ", altura=" + altura + ", peso=" + peso + ", categoria=" + categoria + ", vitorias=" + vitorias + ", derrotas=" + derrotas + ", empates=" + empates + '}';
    }

    public String getNacionalidade() {
        return nacionalidade;
    }

    public void setNacionalidade(String nacionalidade) {
        this.nacionalidade = nacionalidade;
    }

    public int getIdade() {
        return idade;
    }

    public void setIdade(int idade) {
        this.idade = idade;
    }

    public float getAltura() {
        return altura;
    }

    public void setAltura(float altura) {
        this.altura = altura;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
        this.setCategoria();
    }

    public String getCategoria() {
        return categoria;
    }

    private void setCategoria() {
        this.categoria = categoria;
        if(this.peso<52.2){
            this.categoria="inválido";
        }
        else if(this.peso<=70.3){
            this.categoria="leve";
        }
        else if(this.peso<=83.3){
            this.categoria="medio";
        }
        else if(this.peso<=120.2){
            this.categoria="pesado";
            
        }
        else{
            this.categoria="inválido";
        }
    }

    public int getVitorias() {
        return vitorias;
    }

    public void setVitorias(int vitorias) {
        this.vitorias = vitorias;
    }

    public int getDerrotas() {
        return derrotas;
    }

    public void setDerrotas(int derrotas) {
        this.derrotas = derrotas;
    }

    public int getEmpates() {
        return empates;
    }

    public void setEmpates(int empates) {
        this.empates = empates;
    }

    
    
}
